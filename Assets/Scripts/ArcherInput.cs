﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcherInput : MonoBehaviour
{
	[SerializeField]
	private Camera _camera;
	[SerializeField]
	private Arrow _arrow;
	[SerializeField]
	private GameObject _launcher;
	[SerializeField]
	private Vector2 direction;
	[SerializeField]
	private Animator _animator;
	[SerializeField]
	private float _timer;
	
	// Update is called once per frame
	void Update ()
	{
		if (_timer > 0)
		{
			_timer -= Time.deltaTime;
			_animator.SetBool("Attack", false);
		}

		if (_timer <= 0)
		{
			if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
			{
				_animator.SetBool("Attack", true);
				Vector2 vector = _camera.ScreenToWorldPoint(Input.mousePosition);
				direction = new Vector2(vector.x - transform.position.x, vector.y - transform.position.y).normalized;
				Arrow arrow = Instantiate(_arrow);
				arrow.transform.position = _launcher.transform.position;
				arrow.Vector = direction;
				_timer = _animator.GetCurrentAnimatorClipInfo(0).Length / 4.0f;
			}
		}
	}
}
