﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
	public Vector2 Vector = Vector2.left;
	public float Angle = 0.0f;
	public int Damage = 50;
	public float Speed = 50;
	[SerializeField]
	private GameObject _blood;
	
	void Start()
	{
		Destroy(gameObject, 5.0f);
	}

	void Update () {
		// Формула расчета угла стрелы по вектору
		Vector3 angle = new Vector3(0, 0 , (180/Mathf.PI)*((-Vector.y)/(Mathf.Sqrt((Vector.x * Vector.x) + (Vector.y * Vector.y)))));
		Quaternion arrowRotation = Quaternion.Euler(angle);
		
		// Можно также заменить на Rigitbody2D.AddForce() и гравитацию
		Vector.y -= Time.deltaTime / 7;
		transform.Translate(Vector2.left*Time.deltaTime*Speed);
		transform.rotation = arrowRotation;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		Unit unit = other.GetComponent<Unit>();
		if(!unit) 
			return;
		unit.HealthDecrease(Damage);

		GameObject blood = Instantiate(_blood, other.transform.position, other.transform.rotation);
		Destroy(blood, 1.0f);
		Destroy(gameObject);
	}
}
