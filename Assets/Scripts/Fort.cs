﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Fort : MonoBehaviour
{
    public event Action LoseEvent;
    [SerializeField]
    private int _health;
    
    public int Health
    {
        get { return _health; }
        private set
        {  
            if (value > MaxHealth)
            {
                _health = MaxHealth;
                return;
            }

            if (value < 0)
            {
                _health = 0;
                return;
            }
            
            _health = value;
        }
    }
    public int MaxHealth { get; private set; }

    [SerializeField]
    private SpriteRenderer _fortSpriteRenderer;
    [SerializeField]
    private List<Sprite> _fortState;

    public void SetHealth(int maxHealth)
    {
        MaxHealth = maxHealth;
        Health = MaxHealth;
        
        VisualDamage();
    }
    
    public void GetDamage(int damage)
    {
        Health -= damage;
        VisualDamage();
        if(Health == 0)LoseEvent();
    }

    private void Start()
    {
        Health = MaxHealth;
    }

    // Можно вынести в отдельный класс или даже написать MVC или MVVM модель для взаимодействия с визуальными элементами
    void VisualDamage()
    {
        int damaged1 = Mathf.RoundToInt(MaxHealth / 4 * 3);
        int damaged2 = Mathf.RoundToInt(MaxHealth / 2);
        int damaged3 = Mathf.RoundToInt(MaxHealth / 4);

        if (Health >= damaged1)
        {
            _fortSpriteRenderer.sprite = _fortState[0];
        }
        else if(Health < damaged1 && Health >= damaged2)
        {
            _fortSpriteRenderer.sprite = _fortState[1];
        }
        else if(Health < damaged2 && Health >= damaged3)
        {
            _fortSpriteRenderer.sprite = _fortState[2];
        }
        else if(Health == 0)
        {
            _fortSpriteRenderer.sprite = _fortState[3];
        }
    }

}