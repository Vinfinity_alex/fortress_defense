﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSessionController : MonoBehaviour
{
    [SerializeField]
    private GameObject _loseWindow;

    [SerializeField] 
    private Fort _fort;
    [SerializeField] 
    private SpawnController _spawnController;
    [SerializeField] 
    private KillAmountIndicator _killAmountIndicator;

    private void Start()
    {
        _fort.LoseEvent += Lose;
        _spawnController.Complexity = 2;
        _fort.SetHealth(1000);
        _killAmountIndicator._currentKills = 0;
    }

    void Lose()
    {
        SetLoseWindow(true);
        //_spawnController.SpawnSetActive(false);
        Time.timeScale = 0;
    }

    private void SetLoseWindow(bool active)
    {
        _loseWindow.SetActive(active);
    }

    public void Restart()
    {
        SetLoseWindow(false);   
        Time.timeScale = 1;
        _fort.SetHealth(1000);
        _spawnController.ClearLevel();
        _killAmountIndicator._currentKills = 0;
        //_spawnController.SpawnSetActive(true);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }

}