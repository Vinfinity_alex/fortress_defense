﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Fort))]
public class FortHealthIndicator : MonoBehaviour
{
    private Fort _fort;

    [SerializeField] 
    private Image _indicatorImg;

    private void Start()
    {
        _fort = GetComponent<Fort>();
    }

    private void LateUpdate()
    {
        _indicatorImg.fillAmount = (float)_fort.Health / _fort.MaxHealth;
    }
}