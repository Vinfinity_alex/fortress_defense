﻿using UnityEngine;
using UnityEngine.UI;

public class KillAmountIndicator : MonoBehaviour
{
    
    [SerializeField] 
    private Text _indicatorImg;

    public float _currentKills { get; set; }

    private void Start()
    {
        Unit.UnitDeathEvent += UpdateIndicator;
    }

    private void UpdateIndicator()
    {
        _currentKills++;
        _indicatorImg.text = _currentKills.ToString();
    }
}