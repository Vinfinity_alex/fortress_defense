﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(IUnit))]
public class UnitHealthIndicator : MonoBehaviour
{
    private IUnit _unit;

    [SerializeField] 
    private Transform _indicatorImg;

    private void Start()
    {
        _unit = GetComponent<IUnit>();
    }

    private void LateUpdate()
    {
        float ratio = _unit.Health / 100.0f;
        if (ratio >= 0)
        {
            _indicatorImg.localScale = new Vector3(ratio, _indicatorImg.localScale.y, _indicatorImg.localScale.z);
            _indicatorImg.localPosition =
                new Vector3(1 - ratio, _indicatorImg.localPosition.y, _indicatorImg.localPosition.z);
        }
    }
}