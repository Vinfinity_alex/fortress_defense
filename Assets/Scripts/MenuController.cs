﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

	public void StartGame()
	{
		SceneManager.LoadScene(1);
	}

	public void MoreGames()
	{
		Application.OpenURL("https://bitbucket.org/Vinfinity_alex/fortress_defense");
	}

	public void QuitGame()
	{
		Application.Quit();
	}

}
