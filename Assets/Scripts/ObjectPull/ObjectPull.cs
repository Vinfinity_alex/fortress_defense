﻿using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Просто пул обьектов для юнитов,
/// можно сделать более сложную реализацию пула пулов для стрел, крови или других часто появляющихся элементов
/// </summary>
public class ObjectPull : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> Pull;
    [SerializeField]
    private List<GameObject> Unit;
    [SerializeField]
    private Fort _fort;

    public GameObject GetPullItem(int unitType)
    {
        float randomHieght = Random.Range(-3.5f, -1.5f);
        Vector3 height = new Vector2(0, randomHieght);
        Unit unit;
        
        foreach (var item in Pull)
        {
            if (!item.gameObject.active)
            {
                item.transform.position = transform.position + height;
                item.SetActive(true);
                unit = item.GetComponent<Unit>();
                unit.Health = 100;
                return item;
            }
        }

        GameObject newItem = Instantiate(Unit[unitType]);
        unit = newItem.GetComponent<Unit>();
        unit._fort = _fort;
        unit.Health = 100;
        newItem.transform.position = transform.position + height;
        Pull.Add(newItem);

        return newItem;
    }

    public void Clear()
    {
        foreach (var item in Pull)
        {
            item.SetActive(false);
        }
    }
}