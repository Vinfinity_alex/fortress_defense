﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
	[SerializeField] 
	private ObjectPull _objectPull;
	private SpawnController _spawnController;
	
	private WaitForSeconds _waitForSeconds;
	[SerializeField] 
	private float _deley;

	public int Complexity { get; set; }
	
	// Use this for initialization
	void Start ()
	{
//		Complexity = 3;
		_deley = 3;
		_waitForSeconds = new WaitForSeconds(_deley);
		StartCoroutine(SpawnUnit());
	}

	private void Update()
	{
		if (_deley > 0.5f)
		{
			_deley -= Time.deltaTime / 100;
		}
	}

	IEnumerator SpawnUnit()
	{
		while (true)
		{
			yield return _waitForSeconds;

//			byte randA = (byte)Random.Range(0, Complexity);

			int unitType = Random.Range(0, 90);

			int unit = 0;
			if (unitType<30)
			{
				unit = 0;
			}
			if (unitType>=30)
			{
				unit = 1;
			}
			if (unitType>=60)
			{
				unit = 2;
			}
			
			_objectPull.GetPullItem(unit);
		}
	}

	public void ClearLevel()
	{
		_objectPull.Clear();
	}

	public void SpawnSetActive(bool active)
	{
		gameObject.SetActive(active);
	}
}
