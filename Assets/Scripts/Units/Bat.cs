﻿using UnityEngine;

public class Bat : Unit {
    private void OnEnable()
    {
        CurrentState = moveState;
        Damage = 5;
        Health = 50;
    }
}
