﻿using UnityEngine;
/// <summary>
/// Абстакция для юнитов
/// </summary>
public interface IUnit 
{
	int Health { get; set; }
	IState CurrentState { get; set; }
	GameObject GameObject { get; set; }
	float Speed { get; set; }
	int Damage { get; set; }

	void HealthDecrease(int points);

	void Attack();
	void Move();
	void Death();
}
