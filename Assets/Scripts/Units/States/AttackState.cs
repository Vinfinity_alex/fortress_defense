﻿/// <summary>
/// Состояние атаки
/// </summary>
public class AttackState : IState 
{
	public void Update(IUnit unit)
	{
		unit.Attack();
	}
}
