﻿/// <summary>
/// Состояние смерти
/// </summary>
public class DeathState : IState 
{
	public void Update(IUnit unit)
	{
		unit.Death();
	}
}
