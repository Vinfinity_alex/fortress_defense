﻿/// <summary>
/// Абстракция состояний
/// </summary>
public interface IState
{
	void Update(IUnit unit);
}
