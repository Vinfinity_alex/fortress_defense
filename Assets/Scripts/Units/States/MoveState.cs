﻿/// <summary>
/// Состояние движения
/// </summary>
public class MoveState : IState
{
	public void Update(IUnit unit)
	{
		unit.Move();
	}
}