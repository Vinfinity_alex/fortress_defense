﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;
/// <summary>
/// Базовый абстрактный класс юнитов
/// </summary>
public abstract class Unit : MonoBehaviour, IUnit
{

	public static event Action UnitDeathEvent;
	
	#region IUnit
	public int Health { get; set; }
	public IState CurrentState { get; set; }
	public GameObject GameObject { get; set; }
	public float Speed { get; set; }
	public int Damage { get; set; }

	#region Methods
	public void HealthDecrease(int points) { Health -= points; }
	
	// Логику состояний вынес в юнита для меньшей связности что бы получить чистую Стратегию,
	// можно инкапсулировать в состояния и её но тогда получим Стейт Машину, но большую зависимость
	
	public void Attack()
	{
		_animator.SetBool("Attack", true);
		if (timer > 0){ timer -= Time.deltaTime;}
		if (timer <= 0)
		{
			timer = time;
			_fort.GetDamage(Damage);
		}	
		
	}

	public void Move()
	{
		_animator.SetBool("Attack", false);
		GameObject.transform.Translate(Vector3.right*Time.deltaTime*Speed);
	}

	public void Death()
	{
		if (!_animator.GetBool("Death"))
		{
			timer = _animator.GetCurrentAnimatorClipInfo(0).Length / 2.0f;
			UnitDeathEvent();
		}
		_animator.SetBool("Death", true);
		if (timer > 0){ timer -= Time.deltaTime;}
		if (timer <= 0)
		{
			gameObject.SetActive(false);
		}	
	}

	#endregion
	
	#endregion


	public float timer;
	public float time;

	public Fort _fort;
	[SerializeField] 
	protected Animator _animator;
	[SerializeField]
	protected float _speed;
	
	#region Unit states
 	protected MoveState moveState;
	protected AttackState attackState;
	protected DeathState deathState;
	#endregion
	
	void Awake ()
	{
		GameObject = gameObject;
		Speed = _speed;
		moveState = new MoveState();
		attackState = new AttackState();
		deathState = new DeathState();
		CurrentState = moveState;
	}

	void Update ()
	{
		if (Health <= 0) 
			CurrentState = deathState;
		
		CurrentState.Update(this);
	}
	
	private void OnTriggerEnter2D(Collider2D other)
	{
		#if UNITY_EDITOR
			Debug.Log("Attack");
		#endif
		if (other.CompareTag("Fort")) CurrentState = attackState;
	}
}
